<?php

namespace Drupal\views_exposed_groups\Plugin\views\exposed_form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsExposedForm;
use Drupal\views\Plugin\views\exposed_form\ExposedFormPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Provides a views exposed form plugin that groups filters.
 *
 * @ViewsExposedForm(
 *   id = "views_exposed_groups",
 *   title = @Translation("Grouped filters"),
 *   help = @Translation("Groups fields into fieldgroups or vertical tabs."),
 * )
 */
#[ViewsExposedForm(
  id: 'views_exposed_groups',
  title: new TranslatableMarkup('Grouped filters'),
  help: new TranslatableMarkup('Groups fields into fieldgroups or vertical tabs.'),
)]
class ExposedGroups extends ExposedFormPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['groups'] = [
      'default' => [$this->getDefaultGroup()],
    ];
    $options['groups_format'] = ['default' => 'vertical_tabs'];
    $options['groups_vertical_tabs_summary'] = ['default' => 0];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['exposed_groups'] = [
      '#tree' => FALSE,
      '#attributes' => ['id' => 'views-exposed-groups-settings-wrapper'],
    ];

    $form['exposed_groups']['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Render groups as'),
      '#description' => $this->t('Choose the way that the grouped filters will be displayed.'),
      '#parents' => ['exposed_form_options', 'groups_format'],
      '#options' => [
        'vertical_tabs' => $this->t('Vertical tabs'),
        'fieldsets_collapsed' => $this->t('Fieldsets (collapsed)'),
        'fieldsets' => $this->t('Fieldsets (expanded)'),
      ],
      '#default_value' => $this->options['groups_format'],
    ];

    $form['exposed_groups']['vertical_tabs_summary'] = [
      '#type' => 'select',
      '#title' => $this->t('Use vertical tabs summaries'),
      '#description' => $this->t('Enable this option to display a summary of filters on the vertical tab link.'),
      '#parents' => [
        'exposed_form_options',
        'groups_vertical_tabs_summary',
      ],
      '#options' => [
        0 => $this->t('Do not add summaries'),
        1 => $this->t('Add summaries'),
      ],
      '#default_value' => (int) $this->options['groups_vertical_tabs_summary'],
      '#states' => [
        'visible' => [
          [
            ':input[name="exposed_form_options[groups_format]"]' => [
              'value' => 'vertical_tabs',
            ],
          ],
        ],
      ],
    ];

    // Assembles the groups and filters as a table with tabledrag support.
    $headers = [
      ['data' => $this->t('Filter')],
      ['data' => $this->t('Group')],
      ['data' => $this->t('Weight')],
    ];

    $form['exposed_groups']['groups'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#empty' => $this->t('This view does not have any filters.'),
      '#attributes' => [
        'id' => 'views-exposed-groups-table',
      ],
      '#tabledrag' => [],
      '#parents' => ['exposed_form_options', 'groups'],
    ];

    $allFilters = $this->displayHandler->getHandlers('filter');
    $filters = array_filter($allFilters, fn(FilterPluginBase $filter) => $filter->isExposed());
    $weight_delta = round(count($filters) / 2);

    $groups = $this->options['groups'];
    foreach ($groups as $group) {
      $group_name_class_replace = Xss::filter(str_replace('_', '-', $group['name']));
      $form['exposed_groups']['groups']['#tabledrag'][] = [
        'action' => 'match',
        'relationship' => 'sibling',
        'group' => 'filter-group-select',
        'subgroup' => 'filter-group-' . $group_name_class_replace,
        'hidden' => FALSE,
        'limit' => FALSE,
      ];
      $form['exposed_groups']['groups']['#tabledrag'][] = [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'filter-weight',
        'subgroup' => 'filter-weight-' . $group_name_class_replace,
      ];

      $form['exposed_groups']['groups'][$group['name']] = [
        '#attributes' => [
          'colspan' => 4,
          'class' => ['tabledrag-root'],
          'no_striping' => TRUE,
        ],
      ];

      if ($group['name'] === '_none') {
        $form['exposed_groups']['groups']['_none']['title'] = [
          '#plain_text' => $this->t('Not grouped'),
        ];
        $form['exposed_groups']['groups'][$group['name']]['#weight'] = 999;
      }
      else {
        $form['exposed_groups']['groups'][$group['name']]['title'] = [
          '#prefix' => Xss::filter($group['label']),
          '#type' => 'button',
          '#value' => $this->t('Remove group'),
        ];
      }
    }

    $default_group = array_reduce($groups, fn ($ret, $g) => $g['name'] === '_none' ? $g : $ret, $this->getDefaultGroup());
    $group_options = [];
    foreach ($groups as $index => $group) {
      $group_options[$group['name']] = $group['label'] !== NULL ? Xss::filter($group['label']) : $this->t('Not grouped');
    }

    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    foreach ($filters as $filter_name => $filter) {
      $id = $filter->options['expose']['identifier'];
      // Filter is not escaped until t() or Xss::filter() is called.
      $label = $filter->options['expose']['label'];

      // Find the group the filter is currently grouped into.
      $group = array_reduce($groups, function ($ret, $g) use ($id) {
        $has_filter = array_filter($g['filters'], fn ($f) => $f['id'] === $id);
        if (!empty($has_filter)) {
          return $g;
        }
        return $ret;
      }, $default_group);
      $group_name = $group['name'];
      $group_name_class = Xss::filter(str_replace('_', '-', $group_name));
      $grouped_filter = NULL;
      if ($group['filters'] !== NULL) {
        $grouped_filter = array_reduce($group['filters'], function ($result, array $f) use ($id) {
          if ($result !== NULL && $f['id'] === $id) {
            return $f;
          }
          return $result;
        });
      }

      $grouped_filter = $grouped_filter ?? ['id' => $id, 'weight' => 0];
      $weight = $grouped_filter['weight'];
      $form['exposed_groups']['groups']['filter-' . $id] = [
        '#attributes' => ['class' => ['draggable', 'tabledrag-leaf']],
        '#tree' => TRUE,
        '#parents' => [
          'exposed_form_options',
          'filters',
          $id,
        ],
        'filter' => [
          'id' => [
            '#type' => 'value',
            '#value' => $id,
          ],
          'title' => [
            '#plain_text' => $label,
          ],
        ],
        'group' => [
          '#type' => 'select',
          '#title' => $this->t('Group'),
          '#title_display' => 'invisible',
          '#attributes' => [
            'class' => [
              'filter-group-select',
              'filter-group-' . $group_name_class,
            ],
          ],
          '#options' => $group_options,
          '#default_value' => $group_name,
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @label', ['@label' => $label]),
          '#title_display' => 'invisible',
          '#delta' => $weight_delta,
          '#filter_field' => $filter,
          '#default_value' => $weight,
          '#attributes' => [
            'class' => ['filter-weight', 'filter-weight-' . $group_name_class],
          ],
        ],
      ];
    }

    // Add another group button.
    $groups_value = '';
    foreach ($this->options['groups'] as $group_weight => $group) {
      $groups_value .= ($group['name'] !== '_none' ? $group['label'] : '_none') . "\n";
    }
    $groups_value = trim($groups_value);
    $form['exposed_groups']['manage_groups'] = [
      '#tree' => TRUE,
      'groups' => [
        '#type' => 'textarea',
        '#title' => $this->t('Groups'),
        '#rows' => 5,
        '#columns' => 40,
        '#default_value' => Html::escape($groups_value),
        '#element_validate' => [[$this, 'updateGroupsValidate']],
      ],
    ];
  }

  /**
   * Validate the groups text area.
   *
   * @param array &$element
   *   The form element array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public function updateGroupsValidate(array &$element, FormStateInterface $formState) {
    $groups_value = $formState->getValue(['manage_groups', 'groups']);
    $groups = explode("\n", $groups_value);
    $groups = array_map('trim', $groups);

    if (!in_array('_none', $groups)) {
      $formState->setError($element, $this->t('The "_none" group is required.'));
    }

    foreach ($groups as $group_label) {
      $groups_with_label = array_filter($groups, function ($group) use ($group_label) {
        return $group === $group_label;
      });
      if (count($groups_with_label) > 1) {
        $formState->setError($element, $this->t('Duplicate group "@name" found.', ['@name' => $group_label]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    $parents = ['manage_groups', 'groups'];
    $groups_value = $form_state->getValue($parents);
    $new_groups = explode("\n", $groups_value);
    $new_groups = array_map(function ($group_weight, $group_label) {
      $label = trim($group_label);
      return [
        'name' => strtolower(preg_replace('/[^a-zA-Z0-9_]/', '_', $label)),
        'label' => $label,
        'weight' => $group_weight,
        'filters' => [],
      ];
    }, array_keys($new_groups), $new_groups);
    $group_names = array_reduce($new_groups, fn ($ret, $group) => [...$ret, $group['name']], []);
    $filters = $form_state->getValue(['exposed_form_options', 'filters']);

    $none_index = array_search('_none', $group_names);
    foreach ($filters as $id => $filter) {
      $index = array_search($filter['group'], $group_names);
      if ($index === FALSE) {
        // The filter is not in any of the new groups so it needs to be moved to
        // the _none group.
        $index = $none_index;
      }

      $new_groups[$index]['filters'][] = [
        'id' => $id,
        'weight' => $filter['weight'],
      ];
    }

    $this->options['groups'] = $new_groups;
    $form_state->setValue(['exposed_form_options', 'groups'], $new_groups);
    parent::submitOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(&$form, FormStateInterface $form_state) {
    parent::exposedFormAlter($form, $form_state);

    if (!isset($form['#attributes']['class'])) {
      $form['#attributes']['class'] = ['views-exposed-form--views-exposed-group'];
    }
    $form['#attributes']['class'][] = 'views-exposed-form--views-exposed-group';

    if ($this->options['groups_format'] === 'vertical_tabs') {
      $form['filter-tabs'] = [
        '#type' => 'vertical_tabs',
        '#title' => $this->t('Filters'),
        // @todo https://www.drupal.org/project/views_exposed_groups/issues/3154672
        /*
         * '#attached' => [
         *
         *   'library' => ['views_exposed_groups/views_exposed_groups'],
         *   'drupalSettings' => [
         *     'viewsExposedGroups' => [
         *       'id' => $form['#id'],
         *       'options' => $this->options,
         *     ],
         *   ],
         * ],
         */
      ];
    }

    $groups = $this->options['groups'];
    foreach ($groups as $index => $group) {
      // Sets up each group.
      if ($group['name'] !== '_none') {
        $form[$group['name']] = [
          '#type' => 'details',
          '#title' => Xss::filter($group['label']),
          '#open' => $this->options['groups_format'] !== 'fieldsets_collapsed',
          '#weight' => $group['weight'],
        ];

        if ($this->options['groups_format'] === 'vertical_tabs') {
          $form[$group['name']]['#group'] = 'filter-tabs';
        }
      }
      else {
        $form[$group['name']] = [
          '#weight' => $group['weight'],
        ];
      }

      $element = &$form[$group['name']];
      foreach ($group['filters'] as $filter_index => $filter_info) {
        // Adds each filter into the group.
        $id = $filter_info['id'];
        if (isset($form[$id]) && is_array($form[$id])) {
          // Copies the exposed filter form element.
          $element[$id] = $form[$id] + [
            '#weight' => $filter_info['weight'],
            '#title' => $form['#info']['filter-' . $id]['label'],
            '#description' => $form['#info']['filter-' . $id]['description'],
          ];

          if (isset($form[$id . '_op']) && is_array($form[$id . '_op'])) {
            $element[$id . '_op'] = $form[$id . '_op'] + [
              '#weight' => $filter_info['weight'],
            ];
            unset($form[$id . '_op']);
          }

          unset($form[$id]);
        }
        elseif (isset($form[$id . '_wrapper']) && is_array($form[$id . '_wrapper'])) {
          // @todo https://www.drupal.org/project/views_exposed_groups/issues/3154672
        }
      }
    }
  }

  /**
   * Gets the default group with no filters.
   *
   * @return array
   *   An array with name, label and filters keys.
   */
  protected function getDefaultGroup(): array {
    return [
      'name' => '_none',
      'label' => $this->t('Not grouped'),
      'filters' => [],
      'weight' => 0,
    ];
  }

}
