# Views Exposed Groups

Adds a new exposed filter style where exposed filters can be grouped into fieldsets (either traditional fieldsets, collapsed fieldsets or vertical tabs). This is especially useful when a view has a large number of exposed filter options.

## Basic Instructions

To edit the exposed groups in your view,

1. While editing a view, on the right side there is a bold heading Exposed Form. Below is Exposed Form Style, click on the link to the right (for example, "Input Required").
2. Select the radio button next to "Grouped Filters" and click Apply.
3. Scroll to the bottom of the next screen and enter your field group names, one on each line. Pick the type of tabs or fieldsets you want. Click Apply.
4. Click the Grouped Form link on the right side under Exposed Form.
5. Scroll to the bottom of this screen and note that your field group names show up in the drop down next to each field. Assign as needed. Click Apply.

