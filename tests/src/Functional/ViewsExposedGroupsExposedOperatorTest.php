<?php

namespace Drupal\Tests\views_exposed_groups\Functional;

/**
 * Tests a view with exposed groups that also has exposed operators.
 *
 * @group views_exposed_groups
 */
class ViewsExposedGroupsExposedOperatorTest extends ViewsExposedGroupsTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $display = &$this->defaultView->getDisplay('default');
    $display['display_options']['filters']['title']['expose']['use_operator'] = TRUE;
    $display['display_options']['exposed_form'] = [
      'type' => 'views_exposed_groups',
      'options' => [
        'groups' => [
          [
            'name' => 'default',
            'label' => 'Default',
            'weight' => 0,
            'filters' => [
              ['id' => 'title', 'weight' => 0],
            ],
          ],
          [
            'name' => 'advanced',
            'label' => 'Advanced',
            'weight' => 1,
            'filters' => [
              ['id' => 'status', 'weight' => 0],
            ],
          ],
          [
            'name' => '_none',
            'label' => 'Not grouped',
            'weight' => 2,
            'filters' => [
              ['id' => 'uid', 'weight' => 0],
            ],
          ],
        ],
        'groups_format' => 'fieldsets',
        'groups_vertical_tabs_summary' => '0',
      ],
    ];
    $this->defaultView->save();
  }

  /**
   * Asserts that a filter with an exposed operator behaves correctly.
   */
  public function testExposedOperator() {
    $view_path = $this->defaultView->getDisplay('page_1')['display_options']['path'];
    $this->drupalLogin($this->unprivilegedUser);

    $this->drupalGet($view_path);

    // Asserts that the exposed filter with an exposed operator exists.
    $this->assertSession()->fieldExists('title_op');
    $this->assertSession()->fieldExists('title');
    $fieldset_legend = $this->xpath('//fieldset/legend/span[text()="Title"]');
    $this->assertEquals(1, count($fieldset_legend), 'Found the exposed filter label as the fieldset legend.');
  }

}
