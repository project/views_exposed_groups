<?php

namespace Drupal\Tests\views_exposed_groups\Functional;

/**
 * Tests the views user interface when using views_exposed_groups.
 *
 * @group views_exposed_groups
 */
class ViewsExposedGroupsUiTest extends ViewsExposedGroupsTestBase {

  /**
   * Asserts that the a user can group exposed forms.
   */
  public function testViewsUi() {
    $view_path = $this->defaultView->getDisplay('page_2')['display_options']['path'];
    $views_ui_path = '/admin/structure/views/view/' . $this->defaultView->id();
    $this->drupalLogin($this->privilegedUser);

    // Asserts that the view does not have an exposed form.
    $this->drupalGet($view_path);
    $fieldset_links = $this->xpath('//fieldset/legend/span');
    $this->assertEquals(0, count($fieldset_links), 'Found no grouped exposed filters.');

    // Goes to view edit and changes to use exposed groups.
    $this->drupalGet($views_ui_path . '/edit/page_2');
    $this->clickLink('Basic');
    $this->submitForm(['exposed_form[type]' => 'views_exposed_groups'], 'Apply');

    // Adds some groups.
    // @todo Asserts that the groups default to - No group -.
    $edit_form = [
      'exposed_form_options[groups_format]' => 'fieldsets',
      'manage_groups[groups]' => "_none\nFirst\nSecond",
    ];
    $this->submitForm($edit_form, 'Apply');
    $this->assertSession()->linkExists('Grouped filters');

    // Configures the groups.
    $edit_form = [
      'exposed_form_options[filters][status][group]' => 'first',
      'exposed_form_options[filters][title][group]' => 'second',
      'exposed_form_options[filters][uid][group]' => '_none',
    ];
    $exposed_options_path = 'admin/structure/views/nojs/display/' . $this->defaultView->id() . '/page_2/exposed_form_options';
    $this->drupalGet($exposed_options_path);
    $this->submitForm($edit_form, 'Apply');

    // Confirms that the table has the correct rows.
    $this->drupalGet($exposed_options_path);
    $rows = $this->xpath('//table[@id="reorder-group-filters"]/tbody/tr');
    $this->assertEquals(6, count($rows), 'Found six rows in filter group table.');
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][status][group]', 'first');
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][title][group]', 'second');
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][uid][group]', '_none');

    // Saves the view.
    $this->drupalGet($views_ui_path . '/edit');
    $this->submitForm([], 'Save');

    // Checks to make sure that the view now has fieldsets.
    $this->drupalGet($view_path);
    // Note that even though the actual HTML has an anchor element, SimpleTest
    // hides this and so a real xpath will not work.
    $fieldset_links = $this->xpath('//fieldset/legend/span/text()');
    $this->assertEquals(2, count($fieldset_links), 'Found the two fieldset links for grouped filters.');
    $this->assertEquals('First', $fieldset_links[0]->getValue());
    $this->assertEquals('Second', $fieldset_links[1]->getValue());

    // Asserts that the filters work.
    $options = [
      'query' => [
        'uid' => '',
        'nid' => '1',
        'title' => '',
      ],
    ];
    $this->drupalGet($view_path, $options);
    $this->assertViewsTableResultCount(1);

    $options['query']['title'] = substr($this->testNodes[3]->title, 0, 3);
    $this->drupalGet($view_path, $options);
    $this->assertViewsTableResultCount(1);
    $this->assertSession()->pageTextContains('There are no results for your search.');

    $options['query']['nid'] = '';
    $this->drupalGet($view_path, $options);
    $this->assertViewsTableResultCount(1);

    // Goes back to view edit and makes sure that can gracefully remove groups.
    $edit_form = [
      'exposed_form_options[groups_format]' => 'fieldsets',
      'manage_groups[groups]' => "_none",
    ];
    $this->drupalGet($exposed_options_path);
    $this->submitForm($edit_form, 'Apply');
    $this->drupalGet($exposed_options_path);
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][nid][group]', '_none');
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][title][group]', '_none');
    $this->assertSession()->fieldValueEquals('exposed_form_options[filters][uid][group]', '_none');
  }

}
