<?php

namespace Drupal\Tests\views_exposed_groups\Functional;

use Drupal\Core\Config\FileStorage;
use Drupal\Tests\BrowserTestBase;

/**
 * Base test class for views_exposed_groups module tests.
 *
 * @internal
 */
abstract class ViewsExposedGroupsTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'views',
    'views_ui',
    'views_exposed_groups',
  ];

  /**
   * A user account that can create and edit views.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $privilegedUser;

  /**
   * A user account that can access content.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $unprivilegedUser;

  /**
   * Content used in the test, keyed by the entity ID.
   *
   * @var \Drupal\node\Entity\Node[]
   */
  protected $testNodes = [];

  /**
   * A default view to use for testing.
   *
   * @var \Drupal\views\Entity\View
   */
  protected $defaultView;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Creates several random nodes for the view.
    $this->testNodes = $this->createNodes();

    // Creates some user accounts to test with.
    $this->privilegedUser = $this->drupalCreateUser([
      'access administration pages',
      'access content',
      'administer site configuration',
      'administer views',
      'bypass node access',
    ]);
    $this->unprivilegedUser = $this->drupalCreateUser(['access content']);

    // Sets some nice-to-have views configuration.
    $settings = \Drupal::configFactory()->getEditable('views.settings');
    $settings->set('ui.show.advanced_column', TRUE);
    $settings->set('ui.show.default_display', TRUE);
    $settings->save();

    // Adds a view to use for testing.
    $this->defaultView = $this->makeDefaultView();
  }

  /**
   * Create multiple nodes for the test.
   *
   * @param int $count
   *   The number of nodes to create.
   *
   * @return array
   *   An array of node objects keyed by entity ID.
   */
  public function createNodes($count = 5) {
    $content = [];
    for ($i = 0; $i < $count; $i++) {
      $node = $this->drupalCreateNode();
      $content[$node->id()] = $node;
    }
    return $content;
  }

  /**
   * Asserts the number of table rows in the rendered view.
   *
   * @param int $expected
   *   The expected number of rows.
   * @param string $message
   *   A message to use.
   * @param string $group
   *   The assertion group.
   */
  public function assertViewsTableResultCount($expected = 0, $message = '', $group = 'Browser') {
    $message = $message ? $message : 'Found the correct number of views results in the table display.';
    $rows = $this->xpath('//table[contains(@class, :class)]/tbody/tr', [':class' => 'views-table']);
    $this->assertEquals($expected, count($rows), $message, $group);
  }

  /**
   * Creates a default view.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A page displaying content in a table.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeDefaultView() {
    $storage = \Drupal::entityTypeManager()->getStorage('view');
    $file_storage = new FileStorage($this->getModulePath('views_exposed_groups') . '/tests/fixtures');
    $storage
      ->create($file_storage->read('views.view.exposed_groups_test'))
      ->save();

    // Rebuild the router once.
    \Drupal::service('router.builder')->rebuild();

    return $storage->load('exposed_groups_test');
  }

}
