<?php

namespace Drupal\Tests\views_exposed_groups\Functional;

/**
 * Tests a view with exposed groups and autosubmit.
 *
 * @group views_exposed_groups
 */
class ViewsExposedGroupsInheritedPropertiesTest extends ViewsExposedGroupsTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $display = &$this->defaultView->getDisplay('default');
    $display['display_options']['exposed_form'] = [
      'type' => 'views_exposed_groups',
      'options' => [
        'groups' => [
          [
            'name' => 'default',
            'label' => 'Default',
            'weight' => 0,
            'filters' => [
              ['id' => 'title', 'weight' => 0],
            ],
          ],
          [
            'name' => 'advanced',
            'label' => 'Advanced',
            'weight' => 1,
            'filters' => [
              ['id' => 'status', 'weight' => 0],
            ],
          ],
          [
            'name' => '_none',
            'label' => 'Not grouped',
            'weight' => 999,
            'filters' => [
              ['id' => 'uid', 'weight' => 0],
            ],
          ],
        ],
        'groups_format' => 'fieldsets',
        'groups_vertical_tabs_summary' => '0',
      ],
    ];

    $this->defaultView->save();
    $this->assertCount(3, $this->defaultView->getDisplay('default')['display_options']['exposed_form']['options']['groups']);
  }

  /**
   * Asserts form sets values for inherited properties.
   */
  public function testViewsUi() {
    $views_ui_path = '/admin/structure/views/view/' . $this->defaultView->id();
    $this->drupalLogin($this->privilegedUser);

    $this->drupalGet($views_ui_path . '/edit');
    // The simplest way to get to exposed options settings page for a plugin is
    // to click the plugin label and then the "settings" link.
    $this->clickLink('Grouped filters');
    $this->clickLink('settings');
    $edit = [
      'exposed_form_options[reset_button]' => '1',
      // 'exposed_form_options[autosubmit]' => '1',
      // 'exposed_form_options[autosubmit_hide]' => '1',
    ];
    $this->submitForm($edit, 'Apply');

    // Asserts the form options are set.
    $exposed_options_path = 'admin/structure/views/nojs/display/' . $this->defaultView->id() . '/default/exposed_form_options';
    $this->drupalGet($exposed_options_path);

    $this->assertSession()->fieldValueEquals('exposed_form_options[reset_button]', '1');
    // $this->assertSession()->fieldValueEquals('exposed_form_options[autosubmit]', '1');
    // $this->assertSession()->fieldValueEquals('exposed_form_options[autosubmit_hide]', '1');
  }

  /**
   * Asserts that the view itself is displayed correctly.
   *
   * It is not possible to test the actual functionality of the reset
   * and autosubmit functionality.
   */
  public function testView() {
    $view_path = $this->defaultView->getDisplay('page_1')['display_options']['path'];

    // Bypass the UI for this test.
    $display = &$this->defaultView->getDisplay('default');
    $display['display_options']['exposed_form']['options']['reset_button'] = TRUE;

    $this->defaultView->save();
    $this->assertTrue($this->defaultView->getDisplay('default')['display_options']['exposed_form']['options']['reset_button']);

    $this->drupalLogin($this->unprivilegedUser);
    $this->drupalGet($view_path);

    // Asserts that the reset button exists.
    $resetButton = $this->xpath('//input[@value="Reset" and @type="submit"]');
    $this->assertEquals(1, count($resetButton));
  }

}
