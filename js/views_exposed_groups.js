/**
 * @file
 * Views Exposed Groups javascript behaviors.
 */

(function ($, Drupal) {
  Drupal.behaviors.viewsExposedGroups = {
    // @todo Remove unnecessary use of jQuery now that browser support it.
    attach: () => {
      var formId = Drupal.settings.viewsExposedGroups.id;
      var options = Drupal.settings.viewsExposedGroups.options;

      // Provides vertical tab summaries when enabled.
      if (parseInt(options.vertical_tabs_summary, 10)) {
        var $form = $('#' + formId);

        // Gets the immediate wrapper element for each vertical tab pane so that
        // exposed a sort and filter group is processed separately.
        $form.find('.vertical-tabs-pane > .fieldset-wrapper').each(function () {
          var $pane = $(this);
          var $fieldset = $(this.parentNode);

          $fieldset.drupalSetSummary(function () {
            var values = [];

            $pane.children('.form-item').each(function () {
              var value = Drupal.behaviors.viewsExposedGroups.getSummaryForFormItem(this);
              if (value.length > 0) {
                values.push(value);
              }
            });

            $pane.children('.form-wrapper').each(function () {
              var value = Drupal.behaviors.viewsExposedGroups.getSummaryForFormGroup(this);
              if (value.length > 0) {
                values.push(value);
              }
            });

            return values.join('<br />');
          });
        });
      }
    },

    /**
     * Get the summary value for a form-item element.
     *
     * @param {HTMLElement} el the form element.
     *
     * @returns string
     *
     * @internal
     */
    getSummaryForFormItem(el) {
      var value = '';
      var $input = $(el).find(':input');
      var $label = $('label[for="' + $input.attr('id') + '"]');
      var val = $input.val();

      if ($label.length === 1) {
        value += Drupal.checkPlain($label.html()) + ': ';
      }

      if (undefined !== val && val.length > 0) {
        value += Drupal.checkPlain(val);
      }

      return value;
    },

    /**
     * Gets the summary value for a form group.
     *
     * @param {HTMLDivElement} el
     *
     * @returns {string}
     *
     * @internal
     */
    getSummaryForFormGroup(el) {
      var value = '';

      var $label = $(el).find('.fieldset-legend');
      if ($label.length === 1) {
        value += Drupal.checkPlain($label.html()) + '';
      }

      $(el).find(':input').each(function () {
        var $input = $(this);
        var val = $input.val();

        if (undefined !== val && val.length > 0) {
          value += ' ' + Drupal.checkPlain(val);
        }
      });

      return value;
    }
  };
})(jQuery, Drupal);
